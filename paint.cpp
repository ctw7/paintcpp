#include <SFML/Graphics.hpp>

int main()
{
  // create the main window
  sf::RenderWindow window(sf::VideoMode(800, 600), "paintcpp");
  
  // TODO: create toolbar GUI

  // start main loop
  while (window.isOpen()) {
    sf::Event event;
    // process events
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::MouseButtonPressed) {
        if (event.mouseButton.button == sf::Mouse::Left) {
          // TODO: use current tool on mouse click
        }
      }
      // close window
      if (event.type == sf::Event::Closed) {
          window.close();
        }
    }
    
    window.clear();
    window.display();
  }

  return 0;
}
